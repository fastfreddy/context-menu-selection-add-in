Context Menu Selection Add In readme
---------------------


What is this?
-------------

[Context Menu Selection Add In](https://fastfreddy.gitlab.io/context-menu-selection-add-in/) is an extension to the excellent [context-menu-plugin](https://context-menu-plugin.tiddlyhost.com) — Configurable context menus for tiddlers. It adds a few more context menu items that interact with the user selection, from view mode. For example, you can select text from a tiddler, and create a new tiddler with that title. The plugin will also replace the selected text with a wikilink. Another use is to apply formatting or style to the selection, for example highlight, bold, italics, strikethrough or even monoblock. Lastly, if you use Stobot's Sticky plugin or JanJo's ToDo plugin, you will have the option of turning the selected text in a sticky/todo item.

Lastly, the design allows users to rapidly add new context menu items to apply formatting easily. .


See the rest of the plugin's readme [here](https://fastfreddy.gitlab.io/context-menu-selection-add-in/)
